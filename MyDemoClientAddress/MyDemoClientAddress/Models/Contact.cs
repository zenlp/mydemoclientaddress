﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MyDemoClientAddress.Models
{
    public class Contact
    {
        public int ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string CountryId { get; set; }
        public string StateId { get; set; }
        public string Postcode { get; set; }
        public string FullAddress { get; set; }
        public int GridTotalPages { get; set; }
    }
}