﻿; (function ($) {
    var app = {
        cc: {
            $form: $(".needs-validation"),
            $divError: $("#divError"),
            $baseUrl: main.getBaseURL(),
            $contactId: $("#contactId"),
            $firstName: $("#firstName"),
            $lastName : $("#lastName"),
            $middleName: $("#middleName"),
            $phone: $("#phone"),
            $email: $("#email"),
            $address: $("#address"),
            $address2: $("#address2"),
            $country: $("#country"),
            $state: $("#state"),
            $pcode: $("#pcode"),
            $gridBody: $("#tbody"),
            $pageNo: $("#pageNo"),
            $totalPages: $("#totalPages"),
            $spanPageNo: $("#spanPageNo"),
            $spanTotalPages: $("#spanTotalPages"),
            $pageSize: $("#pageSize")
        },

        init: function init() {
            setGridStats();
            app.wireEvents();
        },

        wireEvents: function wireEvents() {

            $("#btnClear").click(function () {
                initialiseScreen();
                app.cc.$form.removeClass("was-validated");
                main.hideElem(app.cc.$divError);
            });

            $("#btnSave").click(function (e) {
                main.hideElem(app.cc.$divError);

                if (!main.validForm(app.cc.$form)) {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                };

                var data = JSON.stringify({
                    contactId: app.cc.$contactId.val(),
                    firstName: app.cc.$firstName.val(),
                    lastName: app.cc.$lastName.val(),
                    middleName: app.cc.$middleName.val(),
                    phone: app.cc.$phone.val(),
                    email: app.cc.$email.val(),
                    address: app.cc.$address.val(),
                    address2: app.cc.$address2.val(),
                    countryid: app.cc.$country.val(),
                    stateid: app.cc.$state.val(),
                    postcode: app.cc.$pcode.val(),
                    pageNumber: app.cc.$pageNo.val(),
                    pageSize: app.cc.$pageSize.val()
                });

                var url = "";
                if (app.cc.$contactId.val().length === 0) {
                    url = app.cc.$baseUrl + "Contact/Insert/";
                    main.ajaxProxy(url, data, updateRow, main.failFn, null, null, "POST");
                } else {
                    url = app.cc.$baseUrl + "Contact/Update/";
                    main.ajaxProxy(url, data, updateRow, main.failFn, null, null, "POST");
                };
                
            });

            $("#tbody").on("click", "#edit", function (e) {
                var row = $(this).closest("tr");
                app.cc.$contactId.val(row.attr("data-contactid"));
                app.cc.$firstName.val(row.attr("data-firstname"));
                app.cc.$middleName.val(row.attr("data-middlename"));
                app.cc.$lastName.val(row.attr("data-lastname"));
                app.cc.$phone.val(row.find("td:eq(1)").text());
                app.cc.$email.val(row.find("td:eq(2)").text());
                app.cc.$address.val(row.attr("data-address"));
                app.cc.$address2.val(row.attr("data-address2"));
                app.cc.$country.val(row.attr("data-countryid"));
                app.cc.$state.val(row.attr("data-stateid"));
                app.cc.$pcode.val(row.attr("data-postcode"));
            });

            $("#tbody").on("click", "#delete", function (e) {
                var data = $(this).closest("tr").attr("data-contactid");
                data = JSON.stringify({ id: data, pageNumber: app.cc.$pageNo.val(), pageSize: app.cc.$pageSize.val() });

                url = app.cc.$baseUrl + "Contact/Delete/";
                main.ajaxProxy(url, data, updateRow, main.failFn, null, null, "POST");
            });

            $("#btnFirst").click(function () {
                app.cc.$pageNo.val(1);
                var data = JSON.stringify({ pageNumber: app.cc.$pageNo.val(), pageSize: app.cc.$pageSize.val() });

                url = app.cc.$baseUrl + "Contact/Select/";
                main.ajaxProxy(url, data, loadGrid, main.failFn, null, null, "POST");
            });

            $("#btnPrev").click(function () {
                var pageNo = parseInt(app.cc.$pageNo.val());
                if (pageNo > 1)
                    pageNo -= 1;

                app.cc.$pageNo.val(pageNo);

                var data = JSON.stringify({ pageNumber: app.cc.$pageNo.val(), pageSize: app.cc.$pageSize.val() });
                url = app.cc.$baseUrl + "Contact/Select/";
                main.ajaxProxy(url, data, loadGrid, main.failFn, null, null, "POST");
            });

            $("#btnNext").click(function () {
                var pageNo = parseInt(app.cc.$pageNo.val());
                var totalPages = parseInt(app.cc.$totalPages.val());
                if (pageNo < totalPages)
                    pageNo += 1;

                app.cc.$pageNo.val(pageNo);

                var data = JSON.stringify({ pageNumber: app.cc.$pageNo.val(), pageSize: app.cc.$pageSize.val() });

                url = app.cc.$baseUrl + "Contact/Select/";
                main.ajaxProxy(url, data, loadGrid, main.failFn, null, null, "POST");
            });

            $("#btnLast").click(function () {
                var totalPages = parseInt(app.cc.$totalPages.val());
                app.cc.$pageNo.val(totalPages);

                var data = JSON.stringify({ pageNumber: app.cc.$pageNo.val(), pageSize: app.cc.$pageSize.val() });

                url = app.cc.$baseUrl + "Contact/Select/";
                main.ajaxProxy(url, data, loadGrid, main.failFn, null, null, "POST");
            });

            $("#pageSize").change(function () {
                var data = JSON.stringify({ pageNumber: app.cc.$pageNo.val(), pageSize: app.cc.$pageSize.val() });
                url = app.cc.$baseUrl + "Contact/Select/";
                main.ajaxProxy(url, data, loadGrid, main.failFn, null, null, "POST");
            });

        }
    };

    initialiseScreen: function initialiseScreen() {
        app.cc.$contactId.val("");
        app.cc.$firstName.val("");
        app.cc.$lastName.val("");
        app.cc.$middleName.val("");
        app.cc.$phone.val("");
        app.cc.$email.val("");
        app.cc.$address.val("");
        app.cc.$address2.val("");
        app.cc.$country.val("");
        app.cc.$state.val("");
        app.cc.$pcode.val("");
    };

    loadGrid: function loadGrid(msg) {
        app.cc.$gridBody.empty();

        if (msg && msg.length > 0) {
            var html = "";
            for (var i = 0; i < msg.length; i++) {
                if (i == 1)
                    app.cc.$totalPages.val(msg[i].GridTotalPages);

                html += main.format("<tr data-contactid='{0}' data-firstname='{1}' data-middlename='{2}' data-lastname='{3}' data-address='{7}' data-address2='{8}' data-countryid='{9}' data-stateid='{10}' data-postcode='{11}'><td>{4}</td><td class='d-none d-md-table-cell'>{5}</td><td class='d-none d-md-table-cell'>{6}</td><td class='d-none d-md-table-cell'>{12}</td><td class='text-center'><a id='edit' href='#' title='Edit'><i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;<a id='delete' href='#' title='Delete'><i class='fa fa-trash'></i></a></td></tr>",
                        msg[i].ContactId,
                        msg[i].FirstName,
                        msg[i].MiddleName,
                        msg[i].LastName,
                        msg[i].FullName,
                        msg[i].Phone,
                        msg[i].Email,
                        msg[i].Address,
                        msg[i].Address2,
                        msg[i].CountryId,
                        msg[i].StateId,
                        msg[i].Postcode,
                        msg[i].FullAddress
                );
            };

            app.cc.$gridBody.append(html);

            setGridStats();
        };
    };

    updateRow: function updateRow(msg, successMessage, callbackParam) {
        app.cc.$form.removeClass("was-validated");
        initialiseScreen();

        if (msg && msg.error) {
            main.showError(app.cc.$divError, msg.error);
            return;
        };

        if (msg) {
            loadGrid(msg);
        };
    };

    setGridStats: function setGridStats() {
        app.cc.$spanPageNo.text(app.cc.$pageNo.val()); 
        app.cc.$spanTotalPages.text(app.cc.$totalPages.val());
    };

    $(document).ready(function () {
        //  Initialise the application once the document is ready
        app.init();
    });
})(jQuery);