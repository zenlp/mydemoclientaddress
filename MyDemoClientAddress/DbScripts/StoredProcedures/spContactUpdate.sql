﻿USE [test]
GO
/****** Object:  StoredProcedure [dbo].[spContactUpdate]    Script Date: 23/05/2018 3:52:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spContactUpdate]
	@ContactId int,
	@FirstName nvarchar(255),
    @LastName nvarchar(255),
    @MiddleName nvarchar(255),
    @Phone nvarchar(255),
    @Email nvarchar(255),
    @Address nvarchar(255),
    @Address2 nvarchar(255),
    @CountryId nvarchar(10),
    @StateId nvarchar(3),
    @Postcode nvarchar(4)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Contact]
	   SET [FirstName] = @FirstName
		  ,[LastName] = @LastName
		  ,[MiddleName] = @MiddleName
		  ,[Phone] = @Phone
		  ,[Email] = @Email
		  ,[Address] = @Address
		  ,[Address2] = @Address2
		  ,[CountryId] = @CountryId
		  ,[StateId] = @StateId
		  ,[Postcode] = @Postcode
	 WHERE ContactId = @ContactId
END
