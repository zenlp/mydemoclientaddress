﻿USE [test]
GO

/****** Object:  StoredProcedure [dbo].[spContactSelect]    Script Date: 24/05/2018 1:08:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spContactSelect]
	@ContactId int null,
	@PageNumber INT = 1,
	@PageSize   INT = 20
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TotalPages int = 0
	DECLARE @Remainder int = 0

	SELECT @TotalPages = COUNT(*) / @PageSize
	FROM Contact

	SET @Remainder = @TotalPages % @PageSize

	IF @Remainder > 0 OR @TotalPages = 0
		SET @TotalPages += 1

	SELECT [ContactId]
		  ,ISNULL([FirstName], '') AS FirstName
		  ,ISNULL([LastName], '') AS LastName
		  ,ISNULL([MiddleName], '') AS MiddleName
		  ,ISNULL([FirstName], '') +
			CASE
				WHEN NOT [FirstName] IS NULL THEN ' '
				ELSE ''
			END +
			ISNULL([MiddleName], '') +					
			CASE
				WHEN NOT [MiddleName] IS NULL THEN ' '
				ELSE ''
			END +
			ISNULL([LastName], '') AS FullName
		  ,ISNULL([Phone], '') AS Phone
		  ,ISNULL([Email], '') AS Email
		  ,ISNULL([Address], '') AS [Address]
		  ,ISNULL([Address2], '') AS Address2
		  ,ISNULL([CountryId], '') AS CountryId
		  ,ISNULL([StateId], '') AS StateId
		  ,ISNULL([Postcode], '') AS Postcode
		  ,ISNULL([Address], '') +
			CASE
				WHEN NOT [Address] IS NULL THEN ' '
				ELSE ''
			END +
			ISNULL([Address2], '') +
			CASE
				WHEN NOT [Address2] IS NULL THEN ' '
				ELSE ''
			END +
			ISNULL([StateId], '') +
			CASE
				WHEN NOT [StateId] IS NULL THEN ' '
				ELSE ''
			END +
			ISNULL([Postcode], '') +
			CASE
				WHEN NOT [Postcode] IS NULL THEN ' '
				ELSE ''
			END +
			ISNULL([CountryId], '') AS FullAddress,
			@TotalPages AS GridTotalPages
	FROM Contact
	WHERE ContactId = COALESCE(@ContactId, ContactId)
	ORDER BY COALESCE(FirstName, '') + COALESCE(MiddleName, '') + COALESCE(LastName, '')
	OFFSET @PageSize * (@PageNumber - 1) ROWS
	FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE);
END
GO