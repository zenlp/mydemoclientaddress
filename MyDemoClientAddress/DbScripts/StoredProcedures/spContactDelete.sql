﻿USE [test]
GO
/****** Object:  StoredProcedure [dbo].[spContactDelete]    Script Date: 23/05/2018 3:52:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spContactDelete]
	@ContactId int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [dbo].[Contact]
		  WHERE ContactId = @ContactId

END
