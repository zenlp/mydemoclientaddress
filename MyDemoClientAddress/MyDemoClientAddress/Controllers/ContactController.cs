﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyDemoClientAddress.Models;
using MyDemoClientAddress.Services;

namespace MyDemoClientAddress.Controllers
{
    public class ContactController : Controller
    {
        private readonly ContactService _contactService;

        public ContactController()
        {
            _contactService = new ContactService();
        }

        // GET: Contact
        [HttpGet]
        public ActionResult Index(int pageNumber = 1)
        {
            var contacts = _contactService.Select(pageNumber, 20);
            return View(contacts);
        }

        [HttpPost]
        public JsonResult Select(int pageNumber, int pageSize)
        {
            return Json(_contactService.Select(pageNumber, pageSize));
        }

        [HttpPost]
        public JsonResult Insert(Contact contact, int pageNumber, int pageSize)
        {
            var errMsg = _contactService.ValidContact(contact);
            if (errMsg.Length > 0)
                return Json(new { error = errMsg });
            
            _contactService.Insert(contact);
            return Json(_contactService.Select(pageNumber));
        }

        [HttpPost]
        public JsonResult Update(Contact contact, int pageNumber, int pageSize)
        {
            var errMsg = _contactService.ValidContact(contact);
            if (errMsg.Length > 0)
                return Json(new { error = errMsg });

            _contactService.Update(contact);
            return Json(_contactService.Select(pageNumber));
        }

        [HttpPost]
        public ActionResult Delete(int id, int pageNumber, int pageSize)
        {
            _contactService.Delete(id);

            return Json(_contactService.Select(pageNumber));

        }
    }
}