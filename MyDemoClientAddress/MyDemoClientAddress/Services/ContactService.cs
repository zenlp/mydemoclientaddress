﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using MyDemoClientAddress.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace MyDemoClientAddress.Services
{
    public class ContactService
    {
        public IEnumerable<Contact> Select(int pageNumber, int pageSize = 20)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString))
            {
                return db.Query<Contact>("spContactSelect",
                    new
                    {
                        ContactId = (int?)null,
                        pageNumber,
                        pageSize
                    },
                    commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public IEnumerable<Contact> Insert(Contact contact)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString))
            {
                return db.Query<Contact>("spContactInsert",
                    new
                    {
                        FirstName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.FirstName == null ? string.Empty : contact.FirstName),
                        LastName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.LastName == null ? string.Empty : contact.LastName),
                        MiddleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.MiddleName == null ? string.Empty : contact.MiddleName),
                        Phone = contact.Phone == null ? string.Empty : contact.Phone,
                        Email = contact.Email == null ? string.Empty : contact.Email,
                        Address = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.Address == null ? string.Empty : contact.Address),
                        Address2 = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.Address2 == null ? string.Empty : contact.Address2),
                        CountryId = contact.CountryId == null ? string.Empty : contact.CountryId,
                        StateId = contact.StateId == null ? string.Empty : contact.StateId,
                        Postcode = contact.Postcode == null ? string.Empty : contact.Postcode
                    },
                        commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Contact> Update(Contact contact)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString))
            {
                return db.Query<Contact>("spContactUpdate",
                    new
                    {
                        ContactId = contact.ContactId,
                        FirstName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.FirstName == null ? string.Empty : contact.FirstName),
                        LastName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.LastName == null ? string.Empty : contact.LastName),
                        MiddleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.MiddleName == null ? string.Empty : contact.MiddleName),
                        Phone = contact.Phone == null ? string.Empty : contact.Phone,
                        Email = contact.Email == null ? string.Empty : contact.Email,
                        Address = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.Address == null ? string.Empty : contact.Address),
                        Address2 = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(contact.Address2 == null ? string.Empty : contact.Address2),
                        CountryId = contact.CountryId == null ? string.Empty : contact.CountryId,
                        StateId = contact.StateId == null ? string.Empty : contact.StateId,
                        Postcode = contact.Postcode == null ? string.Empty : contact.Postcode
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["defaultConnection"].ConnectionString))
            {
                db.Execute("spContactDelete",
                    new { ContactId = id },
                    commandType: CommandType.StoredProcedure);
            }
        }

        public string ValidContact(Contact contact)
        {
            string errMsg = "";

            if (string.IsNullOrEmpty(contact.FirstName))
                errMsg += "Valid first name is required.<br>";

            if (string.IsNullOrEmpty(contact.LastName))
                errMsg += "Valid last name is required.<br>";

            if (string.IsNullOrEmpty(contact.Phone))
                errMsg += "Valid mobile or phone number is required.</br>";

            if (string.IsNullOrEmpty(contact.Address))
                errMsg += "Valid address is required.<br>";

            if (string.IsNullOrEmpty(contact.CountryId))
                errMsg += "Valid country is required.<br>";

            if (string.IsNullOrEmpty(contact.StateId))
                errMsg += "Valid state is required.<br>";

            if (string.IsNullOrEmpty(contact.Postcode))
                errMsg += "Valid post code is required.<br>";

            return errMsg;
        }
    }
}