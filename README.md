# README #

### What is this repository for? ###

* Quick summary
* =============
* Solution was created using Visual Studio 2017 Community Edition and Sql Server 2017 Developer Edition.

* A contact add / edit page demonstrating
* responsive Bootstrap
* Html5 screen field validation
* JQuery
* MVC
* Dapper (ORM)
* Sql Server database design and Stored Procedures

* Version
* https://bitbucket.org/zenlp/mydemoclientaddress/src/master/

### How do I get set up? ###

* Summary of set up
* =================
* Download the solution from here (online BitBucket repository) as a zip file using the left pane menu option Downloads.
* 
* Extract the solution from the zip file and open the solution (.sln file) using Visual Studio.
* 
* Using Sql Server Management Studio Create an sql server database called Test and run in the DbScripts project scripts.
* Run in the Table scripts followed by the Stored Procedure scripts.
* 
* Using Visual Studio Configure the MVC project MyDemoClientAddress database connection string in web.config to have valid credentials to your sql server instance and the Test database.
* 
* Using Visual Studio set the MVC project as the start up project.
* 
* Using Visual Studio recompile the MVC project.
* 
* Using Visual Studio run the MVC project.