﻿USE [test]
GO
/****** Object:  StoredProcedure [dbo].[spContactInsert]    Script Date: 23/05/2018 3:51:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spContactInsert]
	@FirstName nvarchar(255),
    @LastName nvarchar(255),
    @MiddleName nvarchar(255),
    @Phone nvarchar(255),
    @Email nvarchar(255),
    @Address nvarchar(255),
    @Address2 nvarchar(255),
    @CountryId nvarchar(10),
    @StateId nvarchar(3),
    @Postcode nvarchar(4)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Contact]
			   ([FirstName]
			   ,[LastName]
			   ,[MiddleName]
			   ,[Phone]
			   ,[Email]
			   ,[Address]
			   ,[Address2]
			   ,[CountryId]
			   ,[StateId]
			   ,[Postcode])
		 VALUES
			   (@FirstName
			   ,@LastName
			   ,@MiddleName
			   ,@Phone
			   ,@Email
			   ,@Address
			   ,@Address2
			   ,@CountryId
			   ,@StateId
			   ,@Postcode)

END
