﻿; var main = (function ($) {
    var app = {

        getBaseURL: function getBaseURL() {
            var url = location.href;  // entire url including querystring - also: window.location.href;
            var baseURL = url.substring(0, url.indexOf('/', 14));


            if (baseURL.indexOf('http://localhost') != -1) {
                // Base Url for localhost
                //var url = location.href;  // window.location.href;
                //var pathname = location.pathname;  // window.location.pathname;
                //var index1 = url.indexOf(pathname);
                //var index2 = url.indexOf("/", index1 + 1);
                //var baseLocalUrl = url.substr(0, index2);

                return baseURL + "/";
            }
            else {
                // Root Url for domain name
                return baseURL + "/";
            }
        },

        ajaxProxy: function ajaxProxy(url, data, successFn, failFn, successMessage, callbackParam, type) {
            $.ajax({
                type: type, //GET or POST or PUT or DELETE verb
                url: url, // Location of the service
                data: data, //Data sent to server
                contentType: "application/json; charset=utf-8", // content type sent to server
                dataType: "json", //Expected data format from server
                processdata: false, //True or False
                success: function (msg) {//On Successfull service call
                    successFn(msg, successMessage, callbackParam);
                },
                error: function (msg) {
                    failFn(msg, callbackParam); // When Service call fails
                }
            });
        },

        failFn: function failFn(msg, callbackParam) {
            if (callbackParam && callbackParam.failMessage)
                alert(callbackParam.failMessage + msg.responseText);
        },

        // Format a string like c# String.Format() function.
        format: function format(str, coll) {
            coll = typeof coll === 'object' ? coll : Array.prototype.slice.call(arguments, 1);

            return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
                if (m == "{{") { return "{"; }
                if (m == "}}") { return "}"; }
                return coll[n];
            });
        },

        // Bootstrap4 validation approach. Uses form.checkValidity which is HTML5 in browser validation for (attribute) "required" fields
        // on input, textarea, select tags.
        validForm: function validForm(form) {
            var valid = $(form)[0].checkValidity();
            $(form).addClass("was-validated");

            return valid;
        },

        // Show serverside errors in the passed element, typically a DIV tag with a child P tag.
        showError: function showError(elem, error) {
            var p = $(elem).find("P");
            $(p).empty();
            $(p).append(error);
            $(elem).removeClass("d-none");
        },

        // Hide a element.
        hideElem: function hideElem(elem) {
            $(elem).addClass("d-none");
        }
        
    };

    return app;
})(jQuery);